<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 接口常量定义
 * @author 牧羊人
 * @since 2020/4/22
 */
define('MESSAGE_OK', '操作成功');
define('MESSAGE_FAILED', '操作失败');
define('MESSAGE_SYSTEM_ERROR', '系统繁忙，请稍后再试');
define('MESSAGE_PARAMETER_MISSING', '参数丢失');
define('MESSAGE_PARAMETER_ERROR', '参数错误');
define('MESSAGE_PERMISSON_DENIED', '没有权限');
define('MESSAGE_INTERNAL_ERROR', '服务器繁忙，请稍后再试');
define('MESSAGE_NO_TOKEN', 'Token令牌丢失');
define('MESSAGE_TOKEN_FAILED', 'Token令牌验证失败');

define('MESSAGE_NEEDLOGIN', '请登录');
define('MESSAGE_USER_NO_INFO', '您的信息不存在');
define('MESSAGE_USER_FIRBIDDEN', '您已被禁用，请联系管理员');

define('MESSAGE_NO_DEVICEID', '设备号丢失');
define('MESSAGE_NO_DEVICE', '未指定设备');
define('MESSAGE_NO_APPVERSION', '未指定APP版本号');

define('MESSAGE_NO_MOBILE', '请输入手机号');
define('MESSAGE_MOBILE_INVALID', '请输入有效的手机号');
define('MESSAGE_NO_PASSWORD', '请输入密码');
define('MESSAGE_NO_USER_INFO', '用户信息不存在');
define('MESSAGE_PASSWORD_ERROR', '手机号或密码错误');
define('MESSAGE_ACCOUNT_FORBIDDEN', '手机号或密码错误');
define('MESSAGE_MOBILE_REGISTERED', '用户已注册');
define('MESSAGE_NO_VCODE', '请输入验证码');
