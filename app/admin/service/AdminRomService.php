<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;


use app\admin\model\Admin;
use app\admin\model\AdminRom;
use app\admin\model\Menu;
use app\common\service\BaseService;

/**
 * 角色菜单关系-服务类
 * @author 牧羊人
 * @since 2020/7/3
 * Class AdminRomService
 * @package app\admin\service
 */
class AdminRomService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/7/3
     * AdminRomService constructor.
     */
    public function __construct()
    {
        $this->model = new AdminRom();
    }

    /**
     * 获取权限菜单列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since 2020/7/3
     * @author 牧羊人
     */
    public function getList()
    {
        // 类型
        $type = request()->param("type");
        // 类型ID
        $typeId = request()->param('typeId');

        // 获取所有菜单
        $menuMod = new Menu();
        $menuList = $menuMod->getList([], 'sort asc');
        // 获取有权限的菜单
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomMod = new AdminRom();
        $permissionList = $adminRomMod->getList($where, "menu_id asc");
        $checkList = [];
        if ($permissionList) {
            $checkList = array_column($permissionList, "menu_id");
        }
        $list = [];
        if (!empty($menuList)) {
            foreach ($menuList as $val) {
                $data = [];
                $data['id'] = $val['id'];
                $data['name'] = trim($val['name']);
                $data['pId'] = $val['pid'];
                if (in_array($val['id'], $checkList)) {
                    $data['checked'] = true;
                } else {
                    $data['checked'] = false;
                }
                $data['open'] = true;
                $list[] = $data;
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 设置权限
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since 2020/7/3
     * @author 牧羊人
     */
    public function setPermission()
    {
        // 参数
        $param = request()->param();
        // 类型
        $type = intval($param['type']);
        // 类型ID
        $typeId = intval($param['typeId']);
        if (!$typeId) {
            return message("类型ID不能为空", false);
        }
        // 删除现有的权限
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomMod = new AdminRom();
        $permissionList = $adminRomMod->getList($where, "menu_id asc");
        if ($permissionList) {
            $itemList = array_column($permissionList, "id");
            $adminRomMod->deleteDAll($itemList, true);
        }
        // 权限ID
        $authIds = trim($param['authIds']);
        if ($authIds) {
            $itemArr = explode(',', $authIds);
            foreach ($itemArr as $val) {
                $data = [
                    'type' => $type,
                    'type_id' => $typeId,
                    'menu_id' => $val,
                ];
                $adminRomMod = new AdminRom();
                $adminRomMod->edit($data);
            }
        }
        return message("操作成功");
    }

    /**
     * 获取权限菜单列表
     * @param $adminId 用户ID
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @since 2021/1/30
     * @author 牧羊人
     */
    public function getPermissionList($adminId)
    {
        if ($adminId == 1) {
            // 管理员(拥有全部权限)
            $menuModel = new Menu();
            $menuList = $menuModel->getChilds(0);
            return $menuList;
        } else {
            // 非管理员
            $adminMod = new Admin();
            $adminInfo = $adminMod->where("id", $adminId)->find();
            $menuList = $this->model->getPermissionMenu($adminInfo['role_ids'], $adminId, 1, 0);
            return $menuList;
        }
    }

    /**
     * 获取权限节点
     * @param $adminId 用户ID
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @since 2021/2/1
     * @author 牧羊人
     */
    public function getPermissionFuncList($adminId)
    {
        if ($adminId == 1) {
            // 管理员
            $menuModel = new Menu();
            $menuList = $menuModel->where("type", "=", 4)
                ->where("mark", "=", 1)
                ->field("permission")
                ->select()
                ->toArray();
            $permissionList = array_key_value($menuList, "permission");
            return $permissionList;
        } else {
            // 非管理员
            $adminModel = new Admin();
            $adminInfo = $adminModel->getInfo($adminId);
            $permissionList = $this->model->getPermissionFuncList($adminInfo['role_ids'], $adminId);
            return $permissionList;
        }
    }
}