<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\controller;


use app\admin\service\LevelService;
use app\common\controller\Backend;

/**
 * 职级管理-控制器
 * @author 牧羊人
 * @since: 2020/6/30
 * Class Level
 * @package app\admin\controller
 */
class Level extends Backend
{
    /**
     * 初始化方法
     * @author 牧羊人
     * @since: 2020/6/30
     */
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->model = new \app\admin\model\Level();
        $this->service = new LevelService();
    }

    /**
     * 导入Excel
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \think\db\exception\BindParamException
     */
    public function btnImport()
    {
        $error = "";
        // 上传文件(非图片)
        $result = upload_file('file', '', $error);
        if (!$result) {
            return message($error, false);
        }
        // 文件路径
        $filePath = ATTACHMENT_PATH . $result['filePath'];
        if (!file_exists($filePath)) {
            return message("文件不存在", false);
        }
        // 读取文件
        $objPHPExcel = \PHPExcel_IOFactory::load($filePath); //获取sheet表格数目
        $sheetCount = $objPHPExcel->getSheetCount(); //默认选中sheet0表
        $sheetSelected = 0;
        $objPHPExcel->setActiveSheetIndex($sheetSelected); //获取表格行数
        $rowCount = $objPHPExcel->getActiveSheet()->getHighestRow(); //获取表格列数
        $columnCount = $objPHPExcel->getActiveSheet()->getHighestColumn();
        // 计数器
        $totalNum = 0;
        // 循环读取行数据
        for ($row = 2; $row <= $rowCount; $row++) {
            // 获取列值
            $dataArr = array();
            // 名称
            $dataArr['name'] = $objPHPExcel->getActiveSheet()->getCell("A" . $row)->getValue();
            // 状态
            $status = $objPHPExcel->getActiveSheet()->getCell("B" . $row)->getValue();
            $dataArr['status'] = $status == "正常" ? 1 : 2;
            // 排序
            $dataArr['sort'] = $objPHPExcel->getActiveSheet()->getCell("C" . $row)->getValue();
            // 插入数据
            $levemModel = new \app\admin\model\Level();
            $result = $levemModel->edit($dataArr);
            if ($result) {
                $totalNum++;
            }
        }
        return message("本次共导入{$totalNum}条数据", true);
    }

    /**
     * 导出Excel数据
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function btnExport()
    {
        // 参数
        $param = request()->param();
        // 查询条件
        $where = [];
        // 职级名称
        $name = isset($param['name']) ? $param['name'] : "";
        if ($name) {
            $where[] = ["name", "like", "%{$name}%"];
        }
        $where[] = ['mark', "=", 1];
        // 获取导出的数据源
        $list = $this->model->where($where)->select()->toArray();

        // 实例化PHPExcel类
        $objPHPExcel = new \PHPExcel();
        // 激活当前的sheet表
        $objPHPExcel->setActiveSheetIndex(0);
        //5.设置表格头（即excel表格的第一行）
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '职级ID')
            ->setCellValue('B1', '职级名称')
            ->setCellValue('C1', '职级状态')
            ->setCellValue('D1', '职级排序');
        // 设置表格头水平居中
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B1')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('C1')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('D1')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //设置列水平居中
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('B')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('C')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('D')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // 设置单元格宽度
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(10);
        // 循环刚取出来的数组，将数据逐一添加到excel表格。
        for ($i = 0; $i < count($list); $i++) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . ($i + 2), $list[$i]['id']);// 职级ID
            $objPHPExcel->getActiveSheet()->setCellValue('B' . ($i + 2), $list[$i]['name']);// 职级名称
            $objPHPExcel->getActiveSheet()->setCellValue('C' . ($i + 2), $list[$i]['status']);// 职级状态
            $objPHPExcel->getActiveSheet()->setCellValue('D' . ($i + 2), $list[$i]['sort'] == 1 ? "正常" : "停用");// 职级排序
        }
        // 设置保存的Excel表格名称
        $filename = '职级管理_' . date('YmdHis', time()) . ".xlsx";
        // 设置当前激活的sheet表格名称
        $objPHPExcel->getActiveSheet()->setTitle('职级管理');

        // 设置浏览器窗口下载表格
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="' . $filename . '"');
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;
    }

}